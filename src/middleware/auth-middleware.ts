import { NextFunction, Request, Response } from 'express'
import jwt, { JwtPayload } from 'jsonwebtoken'
import { prismaClient } from '../application/database'

const SECRET = process.env.SECRET

const verifyToken = async (req: Request, res: Response, next: NextFunction) => {
    const token = req.header('Authorization')

    if (!token) return res.status(401).json({ errors: 'Unauthenticated' }).end()

    try {
        const decoded = jwt.verify(token, SECRET ?? 'E-Store') as JwtPayload

        req.user = await prismaClient.user.findFirst({
            where: {
                id: decoded.userId
            },
            select: {
                id: true,
                username: true,
                name: true,
                phone: true,
                role: true
            }
        })

        next()
        return
    } catch (error) {
        res.status(401).json({ error: 'Invalid token' }).end()
    }
}

export { verifyToken }
