import { NextFunction, Request, Response } from 'express'
import { LoginRequestBody, RegisterRequestBody } from '../types/auth'
import authService from '../services/auth-service'

const register = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const request: RegisterRequestBody = {
            name: req.body.name,
            username: req.body.username,
            password: req.body.password,
            phone: req.body.phone,
            roleId: req.body.roleId
        }

        const result = await authService.register(request)

        res.status(200).json({
            data: result
        })
    } catch (error) {
        next(error)
    }
}

const login = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const request: LoginRequestBody = {
            username: req.body.username,
            password: req.body.password
        }

        const result = await authService.login(request)

        res.status(200).json({
            data: result.data,
            token: result.token
        })
    } catch (error) {
        next(error)
    }
}

export default { login, register }
