import { NextFunction, Request, Response } from 'express'
import roleService from '../services/role-service'

const create = async function (
    req: Request,
    res: Response,
    next: NextFunction
) {
    try {
        const name = req.body.name

        const result = await roleService.create(name)

        res.status(200).json({
            data: result
        })
    } catch (error) {
        next(error)
    }
}

const list = async function (req: Request, res: Response, next: NextFunction) {
    try {
        const result = await roleService.list()

        res.status(200).json({
            data: result
        })
    } catch (error) {
        next(error)
    }
}

const update = async function (
    req: Request,
    res: Response,
    next: NextFunction
) {
    try {
        const { roleId } = req.params
        const { name } = req.body

        const result = await roleService.update(roleId, name)

        res.status(200).json({
            data: result
        })
    } catch (error) {
        next(error)
    }
}

const remove = async function (
    req: Request,
    res: Response,
    next: NextFunction
) {
    try {
        const { roleId } = req.params

        await roleService.remove(roleId)

        res.status(200).json({
            data: 'OK'
        })
    } catch (error) {
        next(error)
    }
}

export default { create, update, list, remove }
