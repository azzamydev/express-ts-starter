import { Request, Response } from 'express'

const get = async (req: Request, res: Response) => {
    res.status(200).json({
        data: req.user
    })
}

export default { get }
