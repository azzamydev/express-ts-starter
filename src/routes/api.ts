import express from 'express'
import userController from '../controllers/user-controller'
import { verifyToken } from '../middleware/auth-middleware'
import roleController from '../controllers/role-controller'

const apiRouter = express.Router()

apiRouter.use(verifyToken)

apiRouter.get('/api/user', userController.get)

// Role
apiRouter.get('/api/roles', roleController.list)
apiRouter.post('/api/roles', roleController.create)
apiRouter.put('/api/roles/:roleId', roleController.update)
apiRouter.delete('/api/roles/:roleId', roleController.remove)

export { apiRouter }
