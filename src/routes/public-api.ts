import express from 'express'
import authController from '../controllers/auth-controller'

const publicRouter = express.Router()

publicRouter.post('/api/login', authController.login)
publicRouter.post('/api/register', authController.register)

export { publicRouter }
