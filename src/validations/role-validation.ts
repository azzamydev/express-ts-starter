import Joi from 'joi'

const roleNameValidation = Joi.string().max(100).required()

const getRoleValidation = Joi.string().required()

const updateRoleValidation = Joi.object({
    id: Joi.string().required(),
    name: Joi.string().max(100).required()
})

export { roleNameValidation, updateRoleValidation, getRoleValidation }
