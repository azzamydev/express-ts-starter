import { json } from 'body-parser'
import cors from 'cors'
import express, { Application } from 'express'
import { errorMiddleware } from '../middleware/error-middleware'
import { apiRouter } from '../routes/api'
import { publicRouter } from '../routes/public-api'

class Web {
    public app: Application

    constructor() {
        this.app = express()
        this.plugins()
        this.routes()
        this.middleware()
    }

    protected plugins(): void {
        this.app.use(cors())
        this.app.use(express.json())
        this.app.use(json())
    }

    protected middleware(): void {
        this.app.use(errorMiddleware)
    }
    protected routes(): void {
        this.app.use(publicRouter)
        this.app.use(apiRouter)
    }
}

export default Web
