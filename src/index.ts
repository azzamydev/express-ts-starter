import 'dotenv/config'
import { type Application } from 'express'
import Web from './application/web'

if (!process.env.PORT || !process.env.HOST) {
    process.exit(1)
}

const app: Application = new Web().app
const host: string = process.env.HOST
const port: number = parseInt(process.env.PORT)

app.listen(port, () => {
    console.log(`App running on ${host}:${port}`)
})
