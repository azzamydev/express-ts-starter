export type Role = {
    id: string
    name: string
} | null

export type User = {
    id: string
    username: string
    name: string
    phone: string | null
    role: Role
} | null

export type LoginRequestBody = {
    username: string
    password: string
}

export type RegisterRequestBody = {
    username: string
    name: string
    password: string
    phone?: string | null | undefined
    roleId?: string
}

export type RegisterResponse = {
    id: string
    username: string
    name: string
    password: string
    phone?: string | null | undefined
}
