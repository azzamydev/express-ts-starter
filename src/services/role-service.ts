import { prismaClient } from '../application/database'
import { ResponseError } from '../errors/ResponseError'
import {
    getRoleValidation,
    roleNameValidation,
    updateRoleValidation
} from '../validations/role-validation'
import { validate } from '../validations/validation'

const create = async (name: string) => {
    name = validate(roleNameValidation, name)

    const role = await prismaClient.role.findFirst({
        where: {
            name
        }
    })

    if (role) {
        throw new ResponseError(400, 'Role already exists')
    }

    return prismaClient.role.create({
        data: {
            name
        }
    })
}

const list = async () => {
    return prismaClient.role.findMany()
}

const update = async (roleId: string, name: string) => {
    const roleData = validate(updateRoleValidation, {
        id: roleId,
        name
    })

    const role = await prismaClient.role.findFirst({
        where: {
            id: roleData.id
        }
    })

    if (!role) {
        throw new ResponseError(404, 'Role not found')
    }

    return prismaClient.role.update({
        where: {
            id: roleData.id
        },
        data: {
            name: roleData.name
        }
    })
}

const remove = async (roleId: string) => {
    roleId = validate(getRoleValidation, roleId)

    const role = await prismaClient.role.findFirst({
        where: {
            id: roleId
        }
    })

    if (!role) {
        throw new ResponseError(404, 'Role not found')
    }

    return prismaClient.role.delete({
        where: {
            id: roleId
        }
    })
}

export default { create, update, list, remove }
