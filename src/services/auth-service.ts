import { prismaClient } from '../application/database'
import { ResponseError } from '../errors/ResponseError'
import { LoginRequestBody, RegisterRequestBody } from '../types/auth'
import {
    loginUserValidation,
    registerUserValidation
} from '../validations/auth-validation'
import { validate } from '../validations/validation'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'

const SECRET = process.env.SECRET

const register = async (request: RegisterRequestBody) => {
    const user: RegisterRequestBody = validate(registerUserValidation, request)

    if (user.roleId) {
        const countRole = await prismaClient.role.count({
            where: {
                id: request.roleId
            }
        })

        if (countRole !== 1) {
            throw new ResponseError(404, 'Role not found')
        }
    }

    const countUser = await prismaClient.user.count({
        where: {
            username: user.username
        }
    })

    if (countUser === 1) {
        throw new ResponseError(400, 'User already exists')
    }

    user.password = await bcrypt.hash(user.password, 10)

    return prismaClient.user.create({
        data: {
            name: user.name,
            username: user.username,
            password: user.password,
            phone: user.phone,
            roleId: user.roleId
        },
        select: {
            id: true,
            username: true,
            name: true,
            phone: true
        }
    })
}

const login = async (request: LoginRequestBody) => {
    request = validate(loginUserValidation, request)

    const user = await prismaClient.user.findFirst({
        where: {
            username: request.username
        },
        select: {
            id: true,
            username: true,
            name: true,
            phone: true,
            password: true,
            role: true
        }
    })

    if (!user) {
        throw new ResponseError(401, 'Username or Password wrong')
    }

    const isValidatedPassword = await bcrypt.compare(
        request.password,
        user.password
    )

    if (!isValidatedPassword) {
        throw new ResponseError(401, 'Username or Password wrong')
    }

    return {
        data: {
            id: user.id,
            username: user.username,
            name: user.name,
            phone: user.phone,
            role: user.role
        },
        token: jwt.sign(
            { userId: user.id, username: user.username },
            SECRET ?? 'E-Store',
            {
                expiresIn: '1h'
            }
        )
    }
}

export default { register, login }
