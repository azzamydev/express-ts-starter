import supertest from 'supertest'
import Web from '../application/web'
import {
    createManyTestRole,
    createTestRole,
    createTestUser,
    getTestRole,
    removeAllTestRole,
    removeAllTestUser
} from './test-utils'

describe('GET /api/roles', function () {
    beforeEach(async () => {
        await createManyTestRole()
        await createTestUser()
    })

    afterAll(async () => {
        await removeAllTestUser()
        await removeAllTestRole()
    })

    it('should get list roles', async () => {
        const web = new Web().app

        const body = {
            username: 'test',
            password: 'test'
        }

        const login = await supertest(web).post('/api/login').send(body)

        const token = login.body.token

        const result = await supertest(web)
            .get('/api/roles')
            .set('Authorization', token)

        expect(result.status).toBe(200)
        expect(result.body.data.length).toBe(2)
    })
})

describe('POST /api/roles', function () {
    beforeEach(async () => {
        await createTestRole()
        await createTestUser()
    })

    afterAll(async () => {
        await removeAllTestUser()
        await removeAllTestRole()
    })

    it('should can create roles', async () => {
        const web = new Web().app

        const body = {
            username: 'test',
            password: 'test'
        }

        const login = await supertest(web).post('/api/login').send(body)

        const token = login.body.token

        const result = await supertest(web)
            .post('/api/roles')
            .set('Authorization', token)
            .send({
                name: 'test'
            })

        expect(result.status).toBe(200)
        expect(result.body.data.id).toBeDefined()
        expect(result.body.data.name).toBe('test')
    })

    it('should reject if already exist', async () => {
        const web = new Web().app

        const body = {
            username: 'test',
            password: 'test'
        }

        const login = await supertest(web).post('/api/login').send(body)

        const token = login.body.token

        const result = await supertest(web)
            .post('/api/roles')
            .set('Authorization', token)
            .send({
                name: 'Admin'
            })

        expect(result.status).toBe(400)
        expect(result.body.errors).toBeDefined()
    })

    it('should reject if bad request', async () => {
        const web = new Web().app

        const body = {
            username: 'test',
            password: 'test'
        }

        const login = await supertest(web).post('/api/login').send(body)

        const token = login.body.token

        const result = await supertest(web)
            .post('/api/roles')
            .set('Authorization', token)
            .send({
                name: ''
            })

        expect(result.status).toBe(400)
        expect(result.body.errors).toBeDefined()
    })
})

describe('PUT /api/roles/:roleId', function () {
    beforeEach(async () => {
        await createTestRole()
        await createTestUser()
    })

    afterAll(async () => {
        await removeAllTestUser()
        await removeAllTestRole()
    })

    it('should can update role', async () => {
        const web = new Web().app

        const body = {
            username: 'test',
            password: 'test'
        }

        const login = await supertest(web).post('/api/login').send(body)

        const token = login.body.token

        const role = await getTestRole()

        const result = await supertest(web)
            .put(`/api/roles/${role?.id}`)
            .set('Authorization', token)
            .send({
                name: 'test'
            })

        expect(result.status).toBe(200)
        expect(result.body.data.id).toBeDefined()
        expect(result.body.data.name).toBe('test')
    })

    it('should reject if role not found', async () => {
        const web = new Web().app

        const body = {
            username: 'test',
            password: 'test'
        }

        const login = await supertest(web).post('/api/login').send(body)

        const token = login.body.token

        const result = await supertest(web)
            .put(`/api/roles/6599037b4fafe2e38d6ab22e`)
            .set('Authorization', token)
            .send({
                name: 'test'
            })

        expect(result.status).toBe(404)
        expect(result.body.errors).toBeDefined()
    })

    it('should reject if bad request', async () => {
        const web = new Web().app

        const body = {
            username: 'test',
            password: 'test'
        }

        const login = await supertest(web).post('/api/login').send(body)

        const token = login.body.token

        const role = await getTestRole()

        const result = await supertest(web)
            .put(`/api/roles/${role?.id}`)
            .set('Authorization', token)
            .send({
                name: ''
            })

        expect(result.status).toBe(400)
        expect(result.body.errors).toBeDefined()
    })
})

describe('DELETE /api/roles', function () {
    beforeEach(async () => {
        await createTestRole()
        await createTestUser()
    })

    afterAll(async () => {
        await removeAllTestUser()
        await removeAllTestRole()
    })

    it('should can delete role', async () => {
        const web = new Web().app

        const body = {
            username: 'test',
            password: 'test'
        }

        const login = await supertest(web).post('/api/login').send(body)

        const token = login.body.token

        const role = await getTestRole()

        const result = await supertest(web)
            .delete(`/api/roles/${role?.id}`)
            .set('Authorization', token)

        expect(result.status).toBe(200)
        expect(result.body.data).toBe('OK')
    })

    it('should reject if role not found', async () => {
        const web = new Web().app

        const body = {
            username: 'test',
            password: 'test'
        }

        const login = await supertest(web).post('/api/login').send(body)

        const token = login.body.token

        const result = await supertest(web)
            .put(`/api/roles/6599037b4fafe2e38d6ab22e`)
            .set('Authorization', token)
            .send({
                name: 'test'
            })

        expect(result.status).toBe(404)
        expect(result.body.errors).toBeDefined()
    })
})
