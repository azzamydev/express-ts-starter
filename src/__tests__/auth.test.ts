import supertest from 'supertest'
import Web from '../application/web'
import {
    createTestRole,
    createTestUser,
    getTestRole,
    removeAllTestUser,
    removeTestRole
} from './test-utils'

describe('POST /api/register', function () {
    beforeEach(async () => {
        await createTestRole()
    })

    afterAll(async () => {
        await removeAllTestUser()
        await removeTestRole()
    })

    it('should can register', async () => {
        const testRole = await getTestRole()

        const body = {
            username: 'test',
            name: 'test',
            password: 'password',
            phone: '0812345678',
            roleId: testRole?.id
        }

        const result = await supertest(new Web().app)
            .post('/api/register')
            .send(body)

        expect(result.statusCode).toBe(200)
        expect(result.body.data.id).toBeDefined()
        expect(result.body.data.username).toBe('test')
        expect(result.body.data.name).toBe('test')
        expect(result.body.data.phone).toBe('0812345678')
    })

    it('should reject if duplicate user', async () => {
        const testRole = await getTestRole()

        const body = {
            username: 'test',
            name: 'test',
            password: 'password',
            phone: '0812345678',
            roleId: testRole?.id
        }

        const body2 = {
            username: 'test',
            name: 'test',
            password: 'password',
            phone: '0812345678',
            roleId: testRole?.id
        }

        await supertest(new Web().app).post('/api/register').send(body)
        const result = await supertest(new Web().app)
            .post('/api/register')
            .send(body2)

        expect(result.statusCode).toBe(400)
        expect(result.body.errors).toBeDefined()
    })

    it('should reject if role id not exist', async () => {
        const body = {
            username: 'test',
            name: 'test',
            password: 'password',
            phone: '0812345678',
            roleId: '6598ae4cb2ca8a59410f34e2'
        }

        const result = await supertest(new Web().app)
            .post('/api/register')
            .send(body)

        expect(result.statusCode).toBe(404)
        expect(result.body.errors).toBeDefined()
    })
})

describe('POST /api/login', function () {
    beforeEach(async () => {
        await createTestRole()
        await createTestUser()
    })

    afterAll(async () => {
        await removeAllTestUser()
        await removeTestRole()
    })

    it('should can login', async () => {
        const body = {
            username: 'test',
            password: 'test'
        }

        const result = await supertest(new Web().app)
            .post('/api/login')
            .send(body)

        expect(result.status).toBe(200)
        expect(result.body.token).toBeDefined()
        expect(result.body.data.id).toBeDefined()
        expect(result.body.data.username).toBe('test')
        expect(result.body.data.name).toBe('test')
        expect(result.body.data.phone).toBe('0812345678')
    })

    it('should can reject username or password wrong', async () => {
        const body = {
            username: 'test1',
            password: 'test1'
        }

        const result = await supertest(new Web().app)
            .post('/api/login')
            .send(body)

        expect(result.status).toBe(401)
        expect(result.body.errors).toBeDefined()
    })
})
