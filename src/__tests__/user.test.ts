import supertest from 'supertest'
import Web from '../application/web'
import {
    createTestRole,
    createTestUser,
    removeAllTestUser,
    removeTestRole
} from './test-utils'

describe('GET /api/user', function () {
    beforeEach(async () => {
        await createTestRole()
        await createTestUser()
    })

    afterAll(async () => {
        await removeAllTestUser()
        await removeTestRole()
    })

    it('should can get user data', async () => {
        const body = {
            username: 'test',
            password: 'test'
        }

        const login = await supertest(new Web().app)
            .post('/api/login')
            .send(body)

        const token = login.body.token

        const result = await supertest(new Web().app)
            .get('/api/user')
            .set('Authorization', token)

        expect(result.status).toBe(200)
        expect(result.body.data.id).toBeDefined()
        expect(result.body.data.username).toBe('test')
        expect(result.body.data.name).toBe('test')
    })
})
