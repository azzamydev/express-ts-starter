import { prismaClient } from '../application/database'
import bcrypt from 'bcrypt'

export const createTestRole = async () => {
    await prismaClient.role.create({
        data: {
            name: 'Admin'
        }
    })
}

export const createManyTestRole = async () => {
    const roles: string[] = ['Admin', 'User']

    roles.forEach(async (it: string) => {
        await prismaClient.role.create({
            data: {
                name: it
            }
        })
    })
}

export const getTestRole = () => {
    return prismaClient.role.findFirst({
        where: {
            name: 'Admin'
        }
    })
}

export const removeTestRole = async () => {
    await prismaClient.role.deleteMany({
        where: {
            name: 'Admin'
        }
    })
}

export const removeAllTestRole = async () => {
    await prismaClient.role.deleteMany()
}

export const createTestUser = async () => {
    const testRole = await getTestRole()

    await prismaClient.user.create({
        data: {
            username: 'test',
            name: 'test',
            password: await bcrypt.hash('test', 10),
            phone: '0812345678',
            roleId: testRole?.id
        }
    })
}

export const removeAllTestUser = async () => {
    await prismaClient.user.deleteMany({
        where: {
            username: 'test'
        }
    })
}
